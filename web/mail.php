<?php
if (isset($_POST['Email'])) {

    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_to = "christophe.guy99@gmail.com";
    $email_subject = "Mail de Contact Portfolio";

    function problem($error)
    {
        echo json_encode(['error' => $error]);
        die();
    }

    // validation expected data exists
    if (
        !isset($_POST['Name']) ||
        !isset($_POST['Email']) ||
        !isset($_POST['Message'])
    ) {
        problem('Nous sommes désolés, mais il semble y avoir un problème avec le message que vous soumettez.');
    }

    $name = $_POST['Name']; // required
    $email = $_POST['Email']; // required
    $message = $_POST['Message']; // required

    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';

    if (!preg_match($email_exp, $email)) {
        $error_message .= '<li><i class="fa-solid fa-arrow-right"></i>' . html_entity_decode('Entrez un email valide svp') . '</li>';
    }

    $string_exp = "/^[A-Za-z .'-]+$/";

    if (!preg_match($string_exp, $name)) {
        $error_message .= '<li><i class="fa-solid fa-arrow-right"></i>' . html_entity_decode('Entrez un nom valide svp.') . '</li>';
    }

    if (strlen($message) < 2) {
        $error_message .= '<li><i class="fa-solid fa-arrow-right"></i>' . html_entity_decode('Message trop court.') . '</li>';
    }

    if (strlen($error_message) > 0) {
        problem('<ul>' . $error_message . '</ul>');
    }

    $email_message = "Form details below.<br/><br/>";

    function clean_string($string)
    {
        $bad = array("content-type", "bcc:", "to:", "cc:", "href");
        return str_replace($bad, "", $string);
    }

    $email_message .= "Nom: " . clean_string($name) . "<br/>";
    $email_message .= "Email: " . clean_string($email) . "<br/>";
    $email_message .= "Message: " . clean_string($message) . "<br/>";

    // create email headers
    $headers = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
    $headers .= 'From: ' . $email . "\r\n" .
        'Reply-To: ' . $email . "\r\n" .
        'X-Mailer: PHP/' . phpversion();

    @mail($email_to, $email_subject, $email_message, $headers);

    echo json_encode(['success' => "Votre message a bien été envoyé!"]);
}
?>

import designProjects from '../../designprojects.json';

export function createDesign() {
    var section = document.createElement("section");
    section.id = "Design";

    var h3 = document.createElement("h3");
    h3.textContent = "Design";

    var span = document.createElement("span");
    span.className = "blink";
    span.textContent = "_";

    h3.appendChild(span);
    section.appendChild(h3);

    var p = document.createElement("p");
    p.classList.add("design-intro");
    p.innerHTML = "Mes différentes réalisations en design print ou web. Je travaille aussi bien avec la suite Adobe (Photoshop, Illustrator, InDesign, Premiere, AfterEffects, ...), qu'avec Figma, ou bien simplement mon crayon et ma gomme.";
    section.appendChild(p);

    designProjects.forEach(function(project, index) {
        var projectDiv = document.createElement("div");
        projectDiv.classList.add("project");
        projectDiv.id = project.title.replace(/\s+/g, '-').toLowerCase();
    
        var projectContentDiv = document.createElement("div");
        projectContentDiv.classList.add("no-thumbnail");
    
        var projectImageLink = document.createElement("a");
        projectImageLink.href = project.link;
        projectImageLink.classList.add("thumbnail");
        projectImageLink.target = "_blank"; // Open link in a new tab
    
        var projectImage = document.createElement("img");
        projectImage.src = project.imageSrc;
        projectImage.alt = project.title;
    
        var projectTitle = document.createElement("h4");
        projectTitle.textContent = project.title;
    
        var projectDescription = document.createElement("p");
        projectDescription.innerHTML = project.description;
    
        var projectSkillsTitle = document.createElement("h5");
        projectSkillsTitle.textContent = "Compétences:";
    
        var projectSkills = document.createElement("p");
        projectSkills.textContent = project.skills.join(", ");
        var projectSkillsDiv = document.createElement("div");
        projectSkillsDiv.appendChild(projectSkillsTitle);
        projectSkillsDiv.appendChild(projectSkills);
        projectSkillsDiv.classList.add("skills");
    
        projectImageLink.appendChild(projectImage);
        projectContentDiv.appendChild(projectTitle);
        projectContentDiv.appendChild(projectDescription);
        projectContentDiv.appendChild(projectSkillsDiv);
        projectDiv.appendChild(projectImageLink);
        projectDiv.appendChild(projectContentDiv);
    
        // Alternate the placement of project thumbnails between left and right
        if (index % 2 === 0) {
            projectDiv.classList.add("left");
        } else {
            projectDiv.classList.add("right");
        }
    
        section.appendChild(projectDiv);
    });

    var separator= document.createElement("b");
    separator.classList.add("hr", "anim");
    section.appendChild(separator);
    
    return section;
}

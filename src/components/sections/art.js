export function createArt() {
	var section = document.createElement("section");
	section.id = "Art";
  
	var h3 = document.createElement("h3");
	h3.textContent = "Art";
  
	var blinkSpan = document.createElement("span");
	blinkSpan.className = "blink";
	blinkSpan.textContent = "_";
  
	h3.appendChild(blinkSpan);
	section.appendChild(h3);
  
	var pochoirsHeading = document.createElement("h4");
	pochoirsHeading.textContent = "Pochoirs";
  
	var pochoirsParagraph = document.createElement("p");
	pochoirsParagraph.textContent =
	  "Le stencil, ou pochoir en français, est une technique graphique qui permet de reproduire à moindre coût un travail préparatoire en négatif, pour obtenir un résultat en positif. Il met en œuvre plusieurs qualités et techniques incluant digital et manuel.";
	var pochoirsDiv = document.createElement("div");
	pochoirsDiv.appendChild(pochoirsHeading);
	pochoirsDiv.appendChild(pochoirsParagraph);
	pochoirsDiv.classList.add("pochoirs-intro");
  
	var galleryDiv = document.createElement("div");
	galleryDiv.className = "gallery";
  
	var imgContainer1 = document.createElement("div");
	imgContainer1.className = "img-container";
	var img1 = document.createElement("img");
	img1.src = "./assets/img/HAND1.jpg";
	img1.alt = "Pochoir 1";
	imgContainer1.appendChild(img1);
  
	var imgContainer2 = document.createElement("div");
	imgContainer2.className = "img-container";
	var img2 = document.createElement("img");
	img2.src = "./assets/img/HAND2.jpg";
	img2.alt = "Pochoir 2";
	imgContainer2.appendChild(img2);
  
	var imgContainer3 = document.createElement("div");
	imgContainer3.className = "img-container";
	var img3 = document.createElement("img");
	img3.src = "./assets/img/HAND3.jpg";
	img3.alt = "Pochoir 3";
	imgContainer3.appendChild(img3);
  
	galleryDiv.appendChild(imgContainer1);
	galleryDiv.appendChild(imgContainer2);
	galleryDiv.appendChild(imgContainer3);
  
	var reproductionParagraph = document.createElement("p");
	reproductionParagraph.textContent = "Reproduction d'un modèle sur différents supports.";
  
	var techniqueHeading = document.createElement("h4");
	techniqueHeading.id = "tech4";
	techniqueHeading.textContent = "Technique employée";
  
	var techniqueGalleryDiv = document.createElement("div");
	techniqueGalleryDiv.className = "gallery";
  
	var techniqueImgContainer1 = document.createElement("div");
	techniqueImgContainer1.className = "img-container";
	var techniqueImg1 = document.createElement("img");
	techniqueImg1.src = "./assets/img/Decoupe2.jpg";
	techniqueImgContainer1.appendChild(techniqueImg1);
	techniqueImg1.alt = "Découpe du fichier digital imprimé.";
  
	var techniqueImgContainer2 = document.createElement("div");
	techniqueImgContainer2.className = "img-container";
	var techniqueImg2 = document.createElement("img");
	techniqueImg2.src = "./assets/img/Decoupe1.jpg";
	techniqueImgContainer2.appendChild(techniqueImg2);
	techniqueImg2.alt = "Détail de la découpe.";
  
	var techniqueImgContainer3 = document.createElement("div");
	techniqueImgContainer3.className = "img-container";
	var techniqueImg3 = document.createElement("img");
	techniqueImg3.src = "./assets/img/Decoupe6.jpg";
	techniqueImgContainer3.appendChild(techniqueImg3);
	techniqueImg3.alt = "Vérification de l'alignement des 'layers'.";
  
	techniqueGalleryDiv.appendChild(techniqueImgContainer1);
	techniqueGalleryDiv.appendChild(techniqueImgContainer2);
	techniqueGalleryDiv.appendChild(techniqueImgContainer3);
  
	var techniqueParagraph = document.createElement("p");
	techniqueParagraph.id = "technique";
	var techniqueMiniatureSpan = document.createElement("span");
	techniqueMiniatureSpan.className = "miniature";
	var techniqueMiniatureImg = document.createElement("img");
	techniqueMiniatureImg.src = "./assets/img/HELMUTCMJN2.jpg";
	techniqueMiniatureImg.alt = "Détail pochoir";
	techniqueMiniatureSpan.appendChild(techniqueMiniatureImg);
	techniqueParagraph.appendChild(techniqueMiniatureSpan);
	techniqueParagraph.innerHTML +=
	  "La technique utilisée ici implique de décomposer une source digitale dans un logiciel de manipulation d'images (ici, une photo couleur de chat Sphynx dans le logiciel GIMP), puis de filtrer la source en mode d'imprimerie CMJN (Cyan/Magenta/Jaune/Noir), ce qui donne 4 layers (couches) transparentes distinctes, qui empilées l’une sur l’autre dans un ordre précis restitue la richesse des couleurs originales du fichier. Puis est appliqué sur chaque layer un filtre de type 'Trame linéaire' pour obtenir un fichier image découpable physiquement au scalpel. Viens ensuite l'exécution du pochoir à proprement parler, qui est ici effectué à la bombe acrylique, en appliquant de fines couches laissant la transparence de la couche précédente apparaître, pour restituer un maximum de nuances de couleurs du sujet. Il existe d'autres techniques plus classiques comme les pochoirs une couche (rendu \"Noir & Blanc\"), ou l'empilement de couches de couleurs, qui permettent de rendre plus de nuances, mais multiplient les layers utilisés selon le détail demandé.";
  
	var resultDiv = document.createElement("div");
	resultDiv.id = "result";
	resultDiv.classList.add("img-container");
	var resultImg = document.createElement("img");
	resultImg.src = "./assets/img/HELMUTCMJN1.jpg";
	resultImg.alt = "Résultat final.";
	resultDiv.appendChild(resultImg);
	var resultParagraph = document.createElement("p");
	resultParagraph.textContent = "Résultat final.";
	resultDiv.appendChild(resultParagraph);
  
	var techniqueResultDiv = document.createElement("div");
	techniqueResultDiv.id = "technique-result";
	techniqueResultDiv.appendChild(techniqueParagraph);
	techniqueResultDiv.appendChild(resultDiv);
  
	// Create carousel container and add it after techniqueResultDiv
	var carouselContainer = document.createElement("div");
	carouselContainer.classList.add("carousel");
	techniqueResultDiv.insertAdjacentElement("afterend", carouselContainer);

	// Function to import images dynamically
	const importImages = async () => {
	// Import all images from the "carousel1" folder
	const images = await importAll(require.context('../../img/carousel1', false, /\.(png|jpe?g|svg)$/));

	// Count the number of images
	const imageCount = images.length;

	// Iterate over the images and create list items for each image
	images.forEach((image, index) => {
		const listItem = document.createElement("li");
		listItem.style.backgroundImage = `url(${image})`;
		listItem.id = `img-${index}`; // Each image will have a unique id like img-0, img-1, etc.
		carouselContainer.appendChild(listItem);
	});

	console.log(`Number of images: ${imageCount}`);
	};

	// Helper function to import all images from a context
	function importAll(r) {
	return r.keys().map(r);
	}

	// Call the function to import and display the images
	importImages();
	
	
	section.appendChild(h3);
	section.appendChild(pochoirsDiv);
	section.appendChild(galleryDiv);
	section.appendChild(reproductionParagraph);
	section.appendChild(techniqueHeading);
	section.appendChild(techniqueGalleryDiv);
	section.appendChild(techniqueResultDiv);
	section.appendChild(carouselContainer);
  
	var separator = document.createElement("b");
	separator.classList.add("hr", "anim");
	section.appendChild(separator);

	// Wait for all updates to finish before setting up the carousel
    window.onload = () => {
		const carouselContainer = document.querySelector('.carousel');
		
		// Start at the 2nd image
		const images = carouselContainer.getElementsByTagName('li');
		if (images.length > 0) {
			carouselContainer.scrollLeft = images[0].offsetWidth;
			document.getElementById('img-1').classList.add("snapped");
		}
	
		// Setup the carousel
		setupCarousel(carouselContainer);
	};
	
  
	return section;
  }
  
  function setupCarousel(slider) {
    let isDown = false;
    let startX;
    let scrollLeft;
  
    slider.addEventListener('mousedown', e => {
        isDown = true;
        slider.classList.add('active');
        startX = e.pageX - slider.offsetLeft;
        scrollLeft = slider.scrollLeft;
    });
  
    slider.addEventListener('mouseleave', _ => {
        isDown = false;
        slider.classList.remove('active');
    });
  
    slider.addEventListener('mouseup', _ => {
        isDown = false;
        slider.classList.remove('active');
    });
  
    slider.addEventListener('mousemove', e => {
		if (!isDown) return;
		e.preventDefault();
		const x = e.pageX - slider.offsetLeft;
		const SCROLL_SPEED = 2;
		const walk = (x - startX) * SCROLL_SPEED;
		slider.scrollLeft = scrollLeft - walk;
    });
    
}


export function createContact() {
  var section = document.createElement("section");
  section.id = "Contact";
  var h3 = document.createElement("h3");
  h3.textContent = "Contact";
  section.appendChild(h3);

  // create container
  var container = document.createElement("div");

  // form container
  var formContainer = document.createElement("div");
  formContainer.classList.add("form-container");

  // add form to formContainer
  var form = document.createElement("form");
  form.action = "mail.php";
  form.method = "POST";
  form.classList.add("contact-form");

  var nameInput = document.createElement("input");
  nameInput.type = "text";
  nameInput.name = "Name";
  nameInput.placeholder = "Nom";
  form.appendChild(nameInput);

  var emailInput = document.createElement("input");
  emailInput.type = "email";
  emailInput.name = "Email";
  emailInput.placeholder = "Email";
  form.appendChild(emailInput);

  var messageInput = document.createElement("textarea");
  messageInput.placeholder = "Message";
  messageInput.name = "Message";
  form.appendChild(messageInput);

  var buttonContainer = document.createElement("div");
  buttonContainer.classList.add("button-container");

  var submitBtn = document.createElement("button");
  submitBtn.type = "submit";
  submitBtn.innerText = "Envoyer";
  buttonContainer.appendChild(submitBtn);

  var statusSpan = document.createElement("span");
  statusSpan.classList.add("status-message");
  buttonContainer.appendChild(statusSpan);

  // Add event listener for form submission
  form.addEventListener("submit", function (e) {
    e.preventDefault();

    fetch("mail.php", {
      method: "POST",
      body: new FormData(form),
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error("HTTP error " + response.status);
        }
        return response.json();
      })
      .then((data) => {
        // If data.error is present, display it in statusSpan, otherwise display success message
        statusSpan.innerHTML = data.error ? data.error.replace(/\n/g, "<br/>") : "Votre message a bien été envoyé!";
        statusSpan.classList.add("fade-in");
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  });

  buttonContainer.appendChild(statusSpan);
  form.appendChild(buttonContainer);
  formContainer.appendChild(form);
  container.appendChild(formContainer);

  // text container
  var textContainer = document.createElement("div");

  var h4 = document.createElement("h4");
  h4.textContent = "Contactez moi";
  textContainer.appendChild(h4);

  var p = document.createElement("p");
  p.textContent =
  "Je suis disponible pour tout renseignement ou proposition de projet, vous pouvez consulter mon CV ici-même, ou me laisser un message grâce au formulaire ci-contre.";
  textContainer.appendChild(p);
  textContainer.classList.add("contact-text");

  var resumeDiv = document.createElement("div");
  resumeDiv.classList.add("resume-div");

  var resumeLink = document.createElement("a");
  resumeLink.href = "./assets/data/CV_Christophe_GUY.pdf";
  resumeLink.target = "_blank";

  var icon = document.createElement("i");
  icon.classList.add("fa-solid", "fa-file-pdf", "fa-2x");
  icon.setAttribute("aria-hidden", "true");

  var linkText = document.createElement("span");
  linkText.textContent = "C.V.";

  resumeLink.appendChild(icon);
  resumeLink.appendChild(linkText);
  resumeDiv.appendChild(resumeLink);

  textContainer.appendChild(resumeDiv);
  container.appendChild(textContainer);
  container.classList.add("contact-container");


  // Add the container to the section
  section.appendChild(container);

  return section;
}

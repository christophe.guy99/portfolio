import projects from '../../devprojects.json';

export function createDev() {
  var section = document.createElement("section");
  section.id = "Dev";

  var h3 = document.createElement("h3");
h3.textContent = "Dev";

var span = document.createElement("span");
span.className = "blink";
span.textContent = "_";

h3.appendChild(span);
section.appendChild(h3);


  var p = document.createElement("p");
  p.textContent = "Cette section présente mes divers projets et réalisations en Développement Web, lors de projets d'équipe en IUT ou bien personnels lors de stages ou de périodes d'autoformation. Le code de mes travaux est disponible sur ";
  p.classList.add("dev-intro");
  var link = document.createElement("a");
  link.href = "https://gitlab.com/christophe.guy99";
  link.target = "_blank";
  link.textContent = "Gitlab";
  p.appendChild(link);
  p.appendChild(document.createTextNode(". Je travaille généralement sous environnement Windows, mais Linux ne m'est pas inconnu. Mes librairies favorites sont Leaflet, OpenLayers et D3.js. Toujours en veille, je sais m'adapter aux besoins du client et utiliser l'outil adéquat."));
  section.appendChild(p);
  
  

  projects.forEach(function(project, index) {
    var projectDiv = document.createElement("div");
    projectDiv.classList.add("project");
    projectDiv.id = project.title.replace(/\s+/g, '-').toLowerCase();

    var projectContentDiv = document.createElement("div");
    projectContentDiv.classList.add("no-thumbnail");

    var projectImageLink = document.createElement("a");
    projectImageLink.href = project.link;
    projectImageLink.classList.add("thumbnail");
    projectImageLink.target = "_blank"; // Open link in a new tab

    var projectImage = document.createElement("img");
    projectImage.src = project.imageSrc;
    projectImage.alt = project.title;

    var projectTitle = document.createElement("h4");
    projectTitle.textContent = project.title;

    var projectDescription = document.createElement("p");
    projectDescription.textContent = project.description;

    var projectSkillsTitle = document.createElement("h5");
    projectSkillsTitle.textContent = "Compétences:";

    var projectSkills = document.createElement("p");
    projectSkills.textContent = project.skills.join(", ");
    var projectSkillsDiv = document.createElement("div");
    projectSkillsDiv.appendChild(projectSkillsTitle);
    projectSkillsDiv.appendChild(projectSkills);
    projectSkillsDiv.classList.add("skills");


    projectImageLink.appendChild(projectImage);
    projectContentDiv.appendChild(projectTitle);
    projectContentDiv.appendChild(projectDescription);
    projectContentDiv.appendChild(projectSkillsDiv);
    projectDiv.appendChild(projectImageLink);
    projectDiv.appendChild(projectContentDiv);

    // Alternate the placement of project thumbnails between left and right
    if (index % 2 === 0) {
      projectDiv.classList.add("left");
    } else {
      projectDiv.classList.add("right");
    }

    section.appendChild(projectDiv);
  });

  var separator= document.createElement("b");
  separator.classList.add("hr", "anim");
  section.appendChild(separator);
  
  return section;
}

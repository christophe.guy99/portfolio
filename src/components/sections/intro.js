export function createIntro() {
  var section = document.createElement("section");
  section.id = "Intro";

  var h3 = document.createElement("h3");
  h3.textContent = "Intro";

  var span = document.createElement("span");
  span.className = "blink";
  span.textContent = "_";

  h3.appendChild(span);
  section.appendChild(h3);

  var divZ = document.createElement("div");
  divZ.id = "Z";
  
  var pIntro = document.createElement("p");
  pIntro.innerHTML = "Développeur web passionné basé en Haute-Loire, je suis disponible (<em>Open to Work</em>) pour réaliser vos besoins en développement web et/ou web design.<br><br>Mon approche allie créativité et compétences techniques, pour vous fournir des solutions sur-mesure, répondant à vos besoins spécifiques. <br>En Front-end comme en Back-end, je saurai adapter mes outils habituels à votre demande, ou vous conseiller pour le choix des technologies à employer si c'est vos premiers pas dans le monde du web.";
  
  var imgZ = document.createElement("img");
  imgZ.className = "glitch";
  imgZ.src = "./assets/img/Z.png";
  imgZ.alt = "Christophe GUY, Développeur Web.";
  
  // Create a container div for the image and text
  var divContainer = document.createElement("div");
  divContainer.classList.add("image-text-container");
  
  divContainer.appendChild(imgZ);
  divContainer.appendChild(pIntro);
  
  divZ.appendChild(divContainer);
  section.appendChild(divZ);
  
  

  var divZEUM = document.createElement("div");
  divZEUM.id = "ZEUM";

  var h4ZEUM = document.createElement("h4");
  h4ZEUM.textContent = "Z comme ZEUM";

  var pZEUM = document.createElement("p");
  pZEUM.innerHTML =
    "Au début, il y avait le graff <em>(ou graffiti pour les non-initiés)</em>, et cette envie pressante  pour moi de couvrir les murs de couleurs et de messages picturaux. Ce moyen d’expression prit son essor dans les années 90, au moment de mon adolescence, et grava en moi une envie qui dure jusqu’à ce jour, voir et faire voir.<br><br>Pour atteindre ce but, j’ai par la suite développé mes propres protocoles de reproduction, inspirés du graff et du street art, pour répéter mes œuvres sur des supports de type stickers/affiches, et en multiplier la visibilité, grâce à des pochoirs.<br><br>Il y a quelques années, j’ai également découvert la sérigraphie manuelle, affinant ainsi ma connaissance des techniques de reproduction, et poussant plus loin mon envie de partager ces procédés.";

  divZEUM.appendChild(h4ZEUM);
  divZEUM.appendChild(pZEUM);
  section.appendChild(divZEUM);
  divZEUM.classList.add("about-text");

  var divGEEK = document.createElement("div");
  divGEEK.id = "GEEK";

  var divGEEKLeft = document.createElement("div");

  var h4GEEK = document.createElement("h4");
  h4GEEK.textContent = "G comme GEEK";

  var pGEEK = document.createElement("p");
  pGEEK.innerHTML =
    "En parallèle, en 1999, je découvrais le monde de l’internet et percevais bien le potentiel sans limites apparentes du web, un média nouveau et innovateur, à l’époque libre et pas encore corporate.<br><br>Après avoir initialement touché le côté matériel avec une formation de <b>Technicien de Maintenance en Informatique</b>, je devins par la suite féru de graphisme digital. Titulaire d'un <b>Titre Professionnel de Web-Designer</b>, ainsi qu'une <b>Licence Métiers du Numérique</b>, j'ai su affiner mes connaissances de ce média à part entière, pour y apporter ma vision originale.";

  divGEEKLeft.appendChild(h4GEEK);
  divGEEKLeft.appendChild(pGEEK);
  divGEEK.appendChild(divGEEKLeft);
  divGEEKLeft.classList.add("about-text");

  var divGEEKRight = document.createElement("div");

  var imgZOOM = document.createElement("img");
  imgZOOM.src = "./assets/img/ZOOM.jpg";
  imgZOOM.alt = "Détail Graffiti Z";

  divGEEKRight.appendChild(imgZOOM);
  divGEEK.appendChild(divGEEKRight);
  section.appendChild(divGEEK);

  // First new content div with id "NEAR"
  var divNEAR = document.createElement("div");
  divNEAR.id = "NEAR";

  var divImgNEAR = document.createElement("div");

  var imgNEAR = document.createElement("img");
  imgNEAR.src = "./assets/img/@.png";
  imgNEAR.alt = "@ Arobase";
  imgNEAR.id = "NEAR-image";

  divImgNEAR.appendChild(imgNEAR);

  var innerDivNEAR = document.createElement("div");

  var h4NEAR = document.createElement("h4");
  h4NEAR.textContent = "Proche de vous";

  var pNEAR = document.createElement("p");
  pNEAR.textContent =
    "Basé en Haute-Loire, au Puy en Velay, je travaille en indépendant depuis 2023 avec le statut de micro-entrepreneur, et réalise à votre demande différentes prestations telles que création, entretien et refonte, pour touts types de sites web:";

  var ulNEAR = document.createElement("ul");
  var li1NEAR = document.createElement("li");
  var li2NEAR = document.createElement("li");
  var li3NEAR = document.createElement("li");

  var iconHTML = '<i class="fa-solid fa-arrow-right"></i>';

  li1NEAR.innerHTML = `${iconHTML} Site vitrine, centré sur l'image et la mise en avant de votre entreprise ou marque, il est là pour présenter vos produits, concepts et/ou services.`;
  li2NEAR.innerHTML = `${iconHTML} Site marchand, proposant la vente directe de vos produits.`;
  li3NEAR.innerHTML = `${iconHTML} Blog, un site axé sur l'écriture d'articles et permettant à tout un chacun de s'exprimer sur le web. Il est aussi un excellent moyen d'améliorer sa visibilité sur le web.`;

  ulNEAR.appendChild(li1NEAR);
  ulNEAR.appendChild(li2NEAR);
  ulNEAR.appendChild(li3NEAR);

  pNEAR.appendChild(ulNEAR);

  innerDivNEAR.appendChild(h4NEAR);
  innerDivNEAR.appendChild(pNEAR);
  innerDivNEAR.classList.add('about-text');

  divNEAR.appendChild(divImgNEAR);
  divNEAR.appendChild(innerDivNEAR);

  section.appendChild(divNEAR);

  // Second new content div with id "EXPERTISE"
  var divEXPERTISE = document.createElement("div");
  divEXPERTISE.id = "EXPERTISE";

  var divImgEXPERTISE = document.createElement("div");

  var imgEXPERTISE = document.createElement("img");
  imgEXPERTISE.src = "./assets/img/ASCIIEYES.png";
  imgEXPERTISE.alt = "ASCII Eyes";
  imgEXPERTISE.id = "EXPERTISE-image";

  divImgEXPERTISE.appendChild(imgEXPERTISE);

  var innerDivEXPERTISE = document.createElement("div");

  var h4EXPERTISE = document.createElement("h4");
  h4EXPERTISE.textContent = "Savoir-faire";

  var pEXPERTISE = document.createElement("p");
  pEXPERTISE.innerHTML =
    "Je suis en constante veille technologique et je cherche toujours à mettre à profit de nouvelles connaissances dans des technologies telles que HTML5, CSS3, JavaScript, JQuery.<br><br>J’utilise aussi bien des CMS (outils de conception de sites web “tout-en-un”) tels que Wordpress, que seulement mon éditeur de code pour créer pour vous un site sur-mesure.";

  innerDivEXPERTISE.appendChild(h4EXPERTISE);
  innerDivEXPERTISE.appendChild(pEXPERTISE);
  innerDivEXPERTISE.classList.add("about-text");

  divEXPERTISE.appendChild(innerDivEXPERTISE);
  divEXPERTISE.appendChild(divImgEXPERTISE);

  section.appendChild(divEXPERTISE);

  // Third new content div with id "COMPETENCE"
  var divCOMPETENCE = document.createElement("div");
  divCOMPETENCE.id = "COMPETENCE";

  var divImgCOMPETENCE = document.createElement("div");

  var imgCOMPETENCE = document.createElement("img");
  imgCOMPETENCE.src = "./assets/img/ASCIIKANAGAWA.png";
  imgCOMPETENCE.alt = "ASCII KANAGAWA";
  imgCOMPETENCE.id = "COMPETENCE-image";

  divImgCOMPETENCE.appendChild(imgCOMPETENCE);

  var innerDivCOMPETENCE = document.createElement("div");

  var h4COMPETENCE = document.createElement("h4");
  h4COMPETENCE.textContent = "Compétence";

  var pCOMPETENCE = document.createElement("p");
  pCOMPETENCE.innerHTML =
    "Je vous apporterai toute mon expertise dans les domaines suivants:";

  var ulCOMPETENCE = document.createElement("ul");
  var li1COMPETENCE = document.createElement("li");
  var li2COMPETENCE = document.createElement("li");
  var li3COMPETENCE = document.createElement("li");
  var li4COMPETENCE = document.createElement("li");
  var li5COMPETENCE = document.createElement("li");
  var li6COMPETENCE = document.createElement("li");
  var li7COMPETENCE = document.createElement("li");
  var li8COMPETENCE = document.createElement("li");
  var li10COMPETENCE = document.createElement("li");

  var iconHTML = '<i class="fa-solid fa-arrow-right"></i>';

  li1COMPETENCE.innerHTML = `${iconHTML} Conception de charte graphique`;
  li2COMPETENCE.innerHTML = `${iconHTML} Création d’une identité visuelle`;
  li3COMPETENCE.innerHTML = `${iconHTML} Intégration Responsive Design`;
  li4COMPETENCE.innerHTML = `${iconHTML} Création d’un site Wordpress administrable par vos soins`;
  li5COMPETENCE.innerHTML = `${iconHTML} Mise en ligne et Hébergement`;
  li6COMPETENCE.innerHTML = `${iconHTML} Nom de domaine`;
  li7COMPETENCE.innerHTML = `${iconHTML} Référencement naturel (SEO)`;
  li8COMPETENCE.innerHTML = `${iconHTML} Référencement payant (SEA)`;
  li10COMPETENCE.innerHTML = `${iconHTML} Réalisation de visuels print et/ou web`;

  ulCOMPETENCE.appendChild(li1COMPETENCE);
  ulCOMPETENCE.appendChild(li2COMPETENCE);
  ulCOMPETENCE.appendChild(li3COMPETENCE);
  ulCOMPETENCE.appendChild(li4COMPETENCE);
  ulCOMPETENCE.appendChild(li5COMPETENCE);
  ulCOMPETENCE.appendChild(li6COMPETENCE);
  ulCOMPETENCE.appendChild(li7COMPETENCE);
  ulCOMPETENCE.appendChild(li8COMPETENCE);
  ulCOMPETENCE.appendChild(li10COMPETENCE);

  innerDivCOMPETENCE.appendChild(h4COMPETENCE);
  innerDivCOMPETENCE.appendChild(pCOMPETENCE);
  innerDivCOMPETENCE.appendChild(ulCOMPETENCE);
  innerDivCOMPETENCE.classList.add("about-text");

  divCOMPETENCE.appendChild(divImgCOMPETENCE);
  divCOMPETENCE.appendChild(innerDivCOMPETENCE);

  section.appendChild(divCOMPETENCE);

  var separator= document.createElement("b");
  separator.classList.add("hr", "anim");
  section.appendChild(separator);

  return section;
}

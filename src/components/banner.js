import anime from 'animejs';

export function createBanner() {
    const banner = document.createElement("div");
    banner.classList.add("banner");
  
    const grid = [34, 10];
    const col = grid[0];
    const row = grid[1];
    const numberOfElements = col * row;

    for(let i = 0; i < numberOfElements; i++){
      const div = document.createElement("div");
      div.classList.add("animatingElement");
      banner.appendChild(div);
    }
  
    const nav = document.querySelector("nav");
    const parent = nav.parentNode;
    parent.insertBefore(banner, nav);
  
    animateBanner(banner);
  
    return banner;
}

function animateBanner(banner) {
  const elements = Array.from(banner.getElementsByClassName('animatingElement'));
  const scaleAmount = 2.5; // adjust as necessary
  
  const getDistance = (a, b) => {
    const aRect = a.getBoundingClientRect();
    const bRect = b.getBoundingClientRect();
    return Math.hypot(aRect.x - bRect.x, aRect.y - bRect.y);
  }

  const animateRipple = (origin) => {
    origin.classList.add("origin");
    const totalAnimationDuration = 7000;
    const originAnimationDuration = 3000; 

    anime.timeline({
      targets: origin,
      easing: 'easeInOutQuad',
    })
    .add({
      scale: [1, 4],
      backgroundColor: ['#78788c', '#FFFFFF'],
      opacity: [0.6, 1],
      borderColor: ['#D3D3D3', 'rgba(211, 211, 211, 0)'],
      borderWidth: ['0px', '1px'],
      duration: originAnimationDuration,
      complete: () => {
        origin.classList.remove("origin");
      }
    })
    .add({
      scale: [4, 1],
      backgroundColor: ['#FFFFFF', '#78788c'],
      opacity: [1, 0.6],
      borderWidth: ['1px', '0px'],
      duration: originAnimationDuration,
    });

    const maxDistance = Math.max(...elements.map(el => getDistance(origin, el)));
    const delayPerDistance = totalAnimationDuration / maxDistance;

    anime.timeline({
      targets: elements,
      easing: 'easeInOutQuad',
      delay: originAnimationDuration, // Delay the other animations
    })
    .add({
      backgroundColor: [
        { value: '#78788c', duration: 0, easing: 'linear' },
        { value: '#000', duration: totalAnimationDuration/2, easing: 'easeInOutQuad' },
        { value: '#78788c', duration: totalAnimationDuration/2, easing: 'easeInOutQuad' },
      ],
      opacity: [
        { value: [0.9, 0.7], duration: totalAnimationDuration/2, easing: 'easeInOutQuad' },
        { value: [0.7, 0.9], duration: totalAnimationDuration/2, easing: 'easeInOutQuad' },
      ],
      rotateX: [
        { value: ['0turn', '0.6turn'], duration: totalAnimationDuration, easing: 'easeInOutQuad' },
      ],
      scaleX: [1, scaleAmount],
      scaleY: [1, 2],
      skewX: [
        { value: ['0deg', '45deg'], duration: totalAnimationDuration/2, easing: 'easeInOutQuad' },
        { value: ['45deg', '0deg'], duration: totalAnimationDuration/2, easing: 'easeInOutQuad' },
      ],
      rotate: [
        { value: ['0deg', '-45deg'], duration: totalAnimationDuration/2, easing: 'easeInOutQuad' },
        { value: ['-45deg', '0deg'], duration: totalAnimationDuration/2, easing: 'easeInOutQuad' },
      ],
      delay: el => getDistance(origin, el) * delayPerDistance,
      complete: () => {
        setTimeout(() => {
          const newOrigin = elements[Math.floor(Math.random() * elements.length)];
          animateRipple(newOrigin);
        }, originAnimationDuration);
      }
    });
  }

  const initialOrigin = elements[Math.floor(Math.random() * elements.length)];
  animateRipple(initialOrigin);
}

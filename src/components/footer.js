import legalNoticesContent from '../legalnotice.json';


export function createFooter() {
    // create the footer element
    var footer = document.createElement("footer");
  
    // create the footer content container
    var footerContent = document.createElement("div");
    footerContent.classList.add("footer-content");
  
  // create the logo div
  var logo = document.createElement("div");
  logo.classList.add("logo-footer");
  var logoLink = document.createElement("a");
  logoLink.href = "#top";
  var logoImg = document.createElement("img");
  logoImg.src = "./assets/img/logo_w.svg";
  logoImg.alt = "Logo";
  logoLink.appendChild(logoImg);
  logo.appendChild(logoLink);
  footerContent.appendChild(logo);
  
  
    // create the footer links container
    var footerLinks = document.createElement("ul");
    footerLinks.classList.add("footer-links");
  
    // create the footer links
    var navLinks = document.querySelectorAll("nav .nav-links a");
    navLinks.forEach(link => {
      var footerLink = document.createElement("li");
      var a = document.createElement("a");
      a.href = link.href;
      a.textContent = link.textContent;
      footerLink.appendChild(a);
      footerLinks.appendChild(footerLink);
    });
    footerContent.appendChild(footerLinks);
  
// create the social links container
var socialLinks = document.createElement("div");
socialLinks.classList.add("social-links");

var facebookLink = document.createElement("a");
facebookLink.title = "Facebook";  // tooltip text
facebookLink.href = "https://www.facebook.com/ZeumSHS/";
facebookLink.target = "_blank";
facebookLink.innerHTML = '<i class="fa-brands fa-facebook-square fa-xl"></i>';
socialLinks.appendChild(facebookLink);

var linkedinLink = document.createElement("a");
linkedinLink.href = "https://www.linkedin.com/in/christophe-guy-7785b01bb";
linkedinLink.target = "_blank";
linkedinLink.innerHTML = '<i class="fa-brands fa-linkedin-in fa-xl"></i>';
socialLinks.appendChild(linkedinLink);

// New reddit link
var redditLink = document.createElement("a");
redditLink.href = "https://www.reddit.com/user/efka/"; 
redditLink.target = "_blank";
redditLink.innerHTML = '<i class="fa-brands fa-reddit-alien fa-xl"></i>';
socialLinks.appendChild(redditLink);


// New gitlab link
var gitlabLink = document.createElement("a");
gitlabLink.href = "https://gitlab.com/christophe.guy99"; 
gitlabLink.target = "_blank";
gitlabLink.innerHTML = '<i class="fa-brands fa-gitlab fa-xl"></i>';
socialLinks.appendChild(gitlabLink);

// append the social links to the footer content
footerContent.appendChild(socialLinks);

  
// create the copyright text
var copyright = document.createElement("div");
copyright.classList.add("copyright");
copyright.innerHTML = "© " + new Date().getFullYear() + " Christophe GUY";

// Create the Legal Notices link
var legalLink = document.createElement("a");
legalLink.href = "#";
legalLink.innerHTML = "Mentions Légales";
footer.appendChild(legalLink);

// create the legal notices modal
var legalNoticesModal = createLegalNotices(legalNoticesContent, legalLink);
document.body.appendChild(legalNoticesModal);

legalLink.addEventListener("click", function(e) {
  e.preventDefault(); // prevent the default action
  // Display the legal notices modal
  legalNoticesModal.classList.add('show');
  // Disable scrolling on the body element
  document.documentElement.style.overflowY = 'hidden';
  document.body.style.overflowY = 'hidden';
});

// Add an event listener to the document body to close the modal if the user clicks outside of it
window.addEventListener("click", function(event) {
  if (event.target == legalNoticesModal) {
    legalNoticesModal.classList.remove('show');

    // Enable scrolling on the body element
    document.documentElement.style.overflowY = 'auto';
    document.body.style.overflowY = 'auto';
  }
});



// create a new div to hold legalLink and copyright
var copyrightLegalDiv = document.createElement("div");
copyrightLegalDiv.classList.add("copyright-legal-div");

// append the legalLink and copyright to the new div
copyrightLegalDiv.appendChild(copyright);
copyrightLegalDiv.appendChild(legalLink);

// append the footer content and copyrightLegalDiv to the footer
footer.appendChild(footerContent);
footer.appendChild(copyrightLegalDiv);
  
    // return the footer
    return footer;
  }
  
  function createLegalNotices(legalNoticesContent, triggerElement) {
    // create the legal notices modal
    var legalNoticesModal = document.createElement("div");
    legalNoticesModal.classList.add("legal-modal");

    // create the modal content container
    var modalContent = document.createElement("div");
    modalContent.classList.add("modal-content");

    // add a close button
    var closeButton = document.createElement("span");
    closeButton.classList.add("close-button");
    closeButton.innerHTML = "&times;";
    closeButton.addEventListener('click', () => {
        // Hide the modal
        legalNoticesModal.style.display = 'none';

        // Enable scrolling on the body element
        document.documentElement.style.overflowY = 'auto';
        document.body.style.overflowY = 'auto';

        // Reset the right padding and margin of the body
        document.body.style.paddingRight = '0px';
        document.body.style.marginRight = '0px';

        // Reset the right padding of the header and nav
        document.querySelector('header').style.paddingRight = '0px';
        document.querySelector('nav').style.marginRight = '0px';

        // Reset the right position of the back-to-top button
        const backToTopBtn = document.querySelector('#backToTopBtn');
        if (backToTopBtn) {
            backToTopBtn.style.right = "20px";
        }
    });

    // Append the close button to the modal content first
    modalContent.appendChild(closeButton);

    // add a title to the modal
    var modalTitle = document.createElement("h3");
    modalTitle.textContent = "Mentions Légales";
    modalContent.appendChild(modalTitle);

    // Create a div for the scrollable content
    var scrollableContent = document.createElement("div");
    scrollableContent.classList.add("scrollable-content");

    // iterate over the keys of the JSON object
    for (var key in legalNoticesContent) {
        // check if the key actually belongs to the object and not its prototype
        if (legalNoticesContent.hasOwnProperty(key)) {
            var section = legalNoticesContent[key];

            if (section.title) {
                // add the title of the legal notices
                var title = document.createElement("h4");
                title.innerHTML = section.title;
                scrollableContent.appendChild(title);
            }

            if (section.paragraph) {
                // add the paragraph of the legal notices
                var paragraph = document.createElement("p");
                paragraph.innerHTML = section.paragraph;
                scrollableContent.appendChild(paragraph);
            }
        }
    }

    // Append the scrollable content to the modal content
    modalContent.appendChild(scrollableContent);

    // append the modal content to the modal
    legalNoticesModal.appendChild(modalContent);

    // Attach click event to the triggerElement to show the modal
    triggerElement.addEventListener('click', () => {
        // Show the modal
        legalNoticesModal.style.display = 'flex';

        // Disable scrolling on the body element
        document.documentElement.style.overflowY = 'hidden';
        document.body.style.overflowY = 'hidden';

        // Calculate the width of the scrollbar
        const scrollbarWidth = window.innerWidth - document.body.clientWidth;

        // Add the width of the scrollbar to the right padding and margin of the body
        document.body.style.paddingRight = `${scrollbarWidth}px`;
        document.body.style.marginRight = `${scrollbarWidth}px`;

        // Add the width of the scrollbar to the right padding of the header and nav
        document.querySelector('header').style.paddingRight = `${scrollbarWidth}px`;
        document.querySelector('nav').style.marginRight = `-${scrollbarWidth}px`;

        // Add the width of the scrollbar to the right position of the back-to-top button
        const backToTopBtn = document.querySelector('#backToTopBtn');
        if (backToTopBtn) {
            backToTopBtn.style.right = `${parseInt(backToTopBtn.style.right, 10) + scrollbarWidth}px`;
        }
    });

    return legalNoticesModal;
}

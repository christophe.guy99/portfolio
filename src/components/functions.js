import anime from 'animejs';


export function typeWriter(element, callback) {
    var i = 0;
    var txt = element.textContent;
    var speed = 125;
    element.textContent = "";
    function type() {
      if (i < txt.length) {
        element.textContent += txt.charAt(i);
        i++;
        setTimeout(type, speed);
      } else {
        if (callback) {
          callback();
        }
      }
    }
    type();
  }
  
  export function startAnimation() {
    const h1 = document.querySelector("h1");
    const h2 = document.querySelector("h2");
    h2.style.visibility = "hidden";

    let i = 0;
    const h1Text = h1.textContent;
    h1.textContent = '';

    const type = () => {
      if (i < h1Text.length) {
        h1.textContent += h1Text[i];
        i++;
        setTimeout(type, 50);
      } else {
        h2.style.visibility = "visible";
        let j = 0;
        const h2Text = h2.textContent;
        h2.textContent = '';

        const typeH2 = () => {
          if (j < h2Text.length) {
            h2.textContent += h2Text[j];
            j++;
            setTimeout(typeH2, 50);
          } else {
            if (h2.textContent.length > 0) {
              const lastChar = document.createElement("span");
              lastChar.textContent = h2.textContent[h2.textContent.length - 1];
              lastChar.classList.add("blink");
              h2.textContent = h2.textContent.slice(0, -1);
              h2.appendChild(lastChar);
            }
          }
        };
        typeH2();
      }
    };
    type();
  }

export function scrollShrink() {
    if ((window.pageYOffset > 50) && (window.innerWidth > 1000)) {
      document.querySelector("header").style.height = "4rem";
      document.querySelector("nav").style.height = "4rem";
      document.querySelector(".logo img").style.height = "3rem";
      document.querySelector(".logo img").style.width = "3rem";
      document.querySelector(".heading").classList.add("heading-transition");
    } 
    else {
      document.querySelector("header").style.height = "6rem";
      document.querySelector("nav").style.height = "6rem";
      document.querySelector(".logo img").style.height = "4rem";
      document.querySelector(".logo img").style.width = "4rem";
      document.querySelector(".heading").classList.remove("heading-transition");
    }
  }

export function updateBannerHeading(sectionId, bannerHeading) {
    const sectionHeading = document.querySelector(`${sectionId} h3`);
    if(sectionHeading){
      bannerHeading.innerHTML = sectionHeading.innerHTML;
      typeWriter(bannerHeading, function(){
        if(bannerHeading.textContent.length > 0) {
          var lastChar = document.createElement("span");
          lastChar.textContent = bannerHeading.textContent[bannerHeading.textContent.length-1];
          lastChar.classList.add("blink");
          bannerHeading.textContent = bannerHeading.textContent.slice(0,-1);
          bannerHeading.appendChild(lastChar);
        }
      });
    }
  }

  export function createModal() {
    // Create the modal
    var modal = document.createElement("div");
    modal.id = "myModal";
    modal.className = "modal";
  
    // Create the modal content container
    var modalContent = document.createElement("div");
    modalContent.className = "modal-content";
    modal.appendChild(modalContent);
  
    // Create the image tag for modal
    var modalImg = document.createElement("img");
    modalImg.className = "modal-img";
    modalImg.id = "modalImgId";
    modalContent.appendChild(modalImg);
  
    // Append the modal to the body
    document.body.appendChild(modal);
  
    // Get scrollbar width
    const scrollbarWidth = window.innerWidth - document.documentElement.clientWidth;
  
    // Select elements that should adapt
    const header = document.querySelector('header');
    const nav = document.querySelector('nav');
    const backToTopBtn = document.querySelector('#backToTopBtn');
  
    // Add event listener to close modal when clicking outside the image
    modal.addEventListener('click', (e) => {
      if (e.target !== modalImg) {
        modal.classList.remove('show');
        // Enable scrolling on the body element
        document.documentElement.style.overflowY = 'auto';
        document.body.style.overflowY = 'auto';
        document.body.style.paddingRight = '0px';
        header.style.paddingRight = '0px';
        nav.style.marginRight = '0px';
        if (backToTopBtn) {
          backToTopBtn.style.right = "20px";
        }
      }
    });
  
    // Add event listener to close modal when pressing ESC key
    window.addEventListener('keydown', (e) => {
      if (e.key === "Escape") {
        modal.classList.remove('show');
        // Enable scrolling on the body element
        document.documentElement.style.overflowY = 'auto';
        document.body.style.overflowY = 'auto';
        document.body.style.paddingRight = '0px';
        header.style.paddingRight = '0px';
        nav.style.marginRight = '0px';
        if (backToTopBtn) {
          backToTopBtn.style.right = "20px";
        }
      }
    });
  
    // Add click event listener to the document
    document.addEventListener('click', function(e) {
      // Check if the clicked element or its parent has the img-container class
      if (e.target.classList.contains('img-container') || e.target.parentNode.classList.contains('img-container')) {
        // If the clicked element is an image, update the modal's img src
        // If the clicked element is the container itself, get the image inside it
        modalImg.src = e.target.nodeName === 'IMG' ? e.target.src : e.target.querySelector('img').src;
        modal.classList.add('show');
        // Disable scrolling on the body element
        document.documentElement.style.overflowY = 'hidden';
        document.body.style.overflowY = 'hidden';
        document.body.style.paddingRight = `${scrollbarWidth}px`;
        header.style.paddingRight = `${scrollbarWidth}px`;
        nav.style.marginRight = `-${scrollbarWidth}px`;
        if (backToTopBtn) {
          // Access offsetHeight to force a reflow
          void document.body.offsetHeight;
          backToTopBtn.style.right = `${parseInt(backToTopBtn.style.right, 10) + scrollbarWidth}px`;
        }
      }
    });
  }
  

export function createBackToTopButton() {
  // Create a wrapper
  var wrapper = document.createElement("div");
  wrapper.id = "backToTopBtnWrapper";

  // Create a div
  var btn = document.createElement("div");
  btn.id = "backToTopBtn";

  // Create a Font Awesome icon
  var icon = document.createElement("i");
  icon.className = "fa-solid fa-xl fa-arrow-up";

  // Append the icon to the div
  btn.appendChild(icon);
  btn.setAttribute('data-tooltip', 'Retour'); // Set the tooltip text in French

  // Add click event listener to scroll to top
  btn.addEventListener('click', function () {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  });

  // Append the button to the wrapper
  wrapper.appendChild(btn);

  // Append the wrapper to the body
  document.body.appendChild(wrapper);

  // Listen for scroll event to show/hide button
  window.onscroll = function () {
    var btn = document.getElementById("backToTopBtn");
    if (window.pageYOffset > 300) {
        btn.classList.add("visible"); // Use the .visible class when the button should be visible
    } else {
        btn.classList.remove("visible"); // Remove the .visible class when the button should be hidden
    }
  }
}

export function createHeader() {
  const header = document.createElement("header");
  const nav = document.createElement("nav");
  const headlogo = document.createElement("div");
  headlogo.classList.add("headlogo");
  
  const logo = document.createElement("div");
  logo.classList.add("logo");
  const img = document.createElement("img");
  img.src = "./assets/img/logo_b.svg";
  img.alt = "Logo Image";
  logo.appendChild(img);

  const heading = document.createElement("div");
  heading.classList.add("heading");
  const h1 = document.createElement("h1");
  h1.innerText = "C:/hristophe GUY";
  h1.setAttribute("title", "C:/hristophe GUY");
  const h2 = document.createElement("h2");
  h2.id= "banner-text";
  h2.innerText = "Web Dev_";
  heading.appendChild(h1);
  heading.appendChild(h2);
  
  headlogo.appendChild(logo);
  headlogo.appendChild(heading);

  const hamburger = document.createElement("div");
  hamburger.classList.add("hamburger");
  for (let i = 1; i <= 3; i++) {
    const line = document.createElement("div");
    line.classList.add(`line${i}`);
    hamburger.appendChild(line);
  }
  
  const navLinks = document.createElement("ul");
  navLinks.classList.add("nav-links");
  const links = ["Intro", "Dev", "Design", "Art", "Contact"];
  links.forEach(link => {
    const li = document.createElement("li");
    const a = document.createElement("a");
    a.href = `#${link}`;
    a.innerText = link;
    li.appendChild(a);
    navLinks.appendChild(li);
  });
  
  nav.appendChild(headlogo);
  nav.appendChild(hamburger);
  nav.appendChild(navLinks);
  header.appendChild(nav);

  return header;
}

export function toggleNav() {
  const hamburger = document.querySelector(".hamburger");
  const navLinks = document.querySelector(".nav-links");
  const links = document.querySelectorAll(".nav-links li");

  hamburger.addEventListener('click', () => {
    navLinks.classList.toggle("open");
    links.forEach(link => {
      link.classList.toggle("fade");
    });
    hamburger.classList.toggle("toggle");
  });

  links.forEach(link => {
      link.addEventListener('click', () => {
          setTimeout(() => {
              navLinks.classList.remove("open");
              links.forEach(link => {
                  link.classList.remove("fade");
                  link.classList.remove('blurred');
              });
              hamburger.classList.remove("toggle");
          }, 50);
      });

      link.addEventListener('mouseover', () => {
          links.forEach(otherLink => {
              if (otherLink !== link) {
                  otherLink.classList.add('blurred');
              }
          });
      });

      link.addEventListener('mouseout', () => {
          links.forEach(otherLink => {
              if (otherLink !== link) {
                  otherLink.classList.remove('blurred');
              }
          });
      });
  });
}

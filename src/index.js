import '@fortawesome/fontawesome-free/js/fontawesome.js'
import '@fortawesome/fontawesome-free/js/solid.js'
import '@fortawesome/fontawesome-free/js/regular.js'
import '@fortawesome/fontawesome-free/js/brands.js'
import "@fortawesome/fontawesome-free/css/all.min.css";
import './scss/style.scss';
// import 'bootstrap/dist/css/bootstrap.min.css';


//IMPORT CUSTOM FUNCTIONS
import { startAnimation, scrollShrink , createModal, createBackToTopButton } from './components/functions'

//IMPORT HEADER AND ITS LOGIC
import { createHeader } from './components/header.js';
import { toggleNav } from './components/header.js';

//IMPORT BANNER
import { createBanner } from './components/banner.js';


//IMPORT SECTIONS AND CREATE MAIN

import { createIntro } from './components/sections/intro';
import { createDev } from './components/sections/dev';
import { createDesign } from './components/sections/design';
import { createArt } from './components/sections/art';
import { createContact } from './components/sections/contact';

function createMain() {
  var main = document.createElement("main");
  var mainContainer = document.createElement("div");
  mainContainer.id = "main-container";
  main.appendChild(mainContainer);

  mainContainer.appendChild(createIntro());
  mainContainer.appendChild(createDev());
  mainContainer.appendChild(createDesign());
  mainContainer.appendChild(createArt());
  mainContainer.appendChild(createContact());

  return main;
}

//IMPORT FOOTER
import { createFooter } from './components/footer.js';

document.addEventListener("DOMContentLoaded", function() {
  var header = createHeader();
  document.querySelector('body').appendChild(header);
  var banner = createBanner();
  document.querySelector('body').appendChild(banner);
  var main = createMain();
  document.querySelector('body').appendChild(main);
  var footer = createFooter();
  document.querySelector('#main-container').appendChild(footer);
  toggleNav();
  createBackToTopButton();
  createModal();
});



document.addEventListener('DOMContentLoaded', function() {
  startAnimation();
});
window.onscroll = function() {scrollShrink();}

document.addEventListener("contextmenu", function(e) {
  if (e.target.nodeName === "IMG" || e.target.closest(".legal-modal")) {
    e.preventDefault();
  }
}, false);


function ctrlShiftKey(e, keyCode) {
  return e.ctrlKey && e.shiftKey && e.keyCode === keyCode.charCodeAt(0);
}
// Disable right-click
document.addEventListener('contextmenu', (e) => e.preventDefault());
document.onkeydown = (e) => {
  // Disable F12, Ctrl + Shift + I, Ctrl + Shift + J, Ctrl + U
  if (
    event.keyCode === 123 ||
    ctrlShiftKey(e, 'I') ||
    ctrlShiftKey(e, 'J') ||
    ctrlShiftKey(e, 'C') ||
    (e.ctrlKey && e.keyCode === 'U'.charCodeAt(0))
  ) {
    // Get all the section elements
    const sections = document.getElementsByTagName('section');
    
    // Iterate through the sections and add the shake class
    for (let i = 0; i < sections.length; i++) {
      sections[i].classList.add('shake-effect');
    }
    
    // Remove the shake class after a certain duration
    setTimeout(() => {
      for (let i = 0; i < sections.length; i++) {
        sections[i].classList.remove('shake-effect');
      }
    }, 1000); // Adjust the duration as desired
    return false;
  }
};

$(document).on('click', 'a[href^="#Intro"]', function (event) {
  event.preventDefault();
  var headerHeight = $("header").height(); // get current header height
  var offset = $.attr(this, 'href') === "#Intro" ? 200 : headerHeight; // set offset based on condition
  $('html, body').animate({
  scrollTop: $($.attr(this, 'href')).offset().top - offset
  }, 200);
  });

  
  document.addEventListener('DOMContentLoaded', function() {
    const bannerHeading = document.querySelector('#banner-text');
  
    window.addEventListener('scroll', function() {
      let scrollPosition = window.pageYOffset || document.documentElement.scrollTop;
      let aboveThreshold = false;
      
      const sections = document.querySelectorAll('section');
      sections.forEach(function(section) {
        const sectionPosition = section.offsetTop;
  
        if (sectionPosition - scrollPosition <= parseFloat(getComputedStyle(document.documentElement).fontSize) * 6) {
          aboveThreshold = true;
          bannerHeading.innerHTML = section.querySelector('h3').innerHTML;
        }
      });
  
      if (!aboveThreshold) {
        bannerHeading.innerHTML = 'Web Dev_';
      }
    });
  });
  
const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require("copy-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');

module.exports = {
  mode: 'none',
  entry: {
    index: {
      import: './src/index.js',
      dependOn: 'shared',
    },
    shared: 'lodash',
  },
  output: {
    filename: 'js/[name].bundle.js',
    path: path.resolve(__dirname, 'web'),
    assetModuleFilename: '[name][ext][query]',
    clean: true,
  },
  optimization: {
    runtimeChunk: 'single',
  },
  devtool: 'inline-source-map',
  devServer: {
    static: './web',
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Christophe Guy, Développeur Web. - Portfolio',
      meta: {
        viewport: 'width=device-width, initial-scale=1',
        description: { name: 'description', content: 'Ce site vous présente mes différentes réalisations dans le domaine du Web dev et du Web design. Vous y trouverez également le lien vers mes différents réseaux pour me contacter.' },
      },
      template: './src/index.html'
    }),
    new CopyPlugin({
      patterns: [
        { from: "./src/img/", to: "./assets/img/" },
        { from: "./src/mail.php", to: "./" },
        { from: "./src/CV_Christophe_GUY.pdf", to: "./assets/data/" },
        { from: "./src/MOST.pdf", to: "./assets/data/" },
      ],
      options: {
        concurrency: 100,
      },
    }),
    new MiniCssExtractPlugin({
      filename: "./assets/css/[name].css",
      chunkFilename: "[id].css"
    }),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      "window.jQuery": "jquery",
      Popper: ['popper.js', 'default'],
    }),
    new FaviconsWebpackPlugin({
      logo: './src/img/logo_b.svg',
      prefix: 'assets/img/',
      mode: 'auto',
      devMode: 'light',
      favicons: {
        appName: 'Portfolio',
        appDescription: 'https://devguy.fr/, Ze Dev Guy: Portfolio de Christophe GUY',
        developerName: 'C. Guy',
        developerURL: null,
        background: '#ddd',
        theme_color: '#333',
        icons: {
          android: true,
          coast: false,
          yandex: false
        }
      }
    })
  ],
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: [MiniCssExtractPlugin.loader, "css-loader"],
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          MiniCssExtractPlugin.loader,
          "css-loader",
          "sass-loader"
        ],
      },
      {
        test: /.*\.(gif|png|jpe?g|svg)$/i,
        type: 'asset/resource',
        generator: {
          filename: 'assets/img/[name][ext][query]'
        }
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/i,
        type: 'asset/resource',
        generator: {
          filename: 'assets/fonts/[name][ext][query]'
        }
      },
      {
        test: /\.(csv|tsv)$/i,
        type: 'asset/resource',
        generator: {
          filename: 'assets/data/[name][ext][query]'
        }
      },
      {
        test: /\.xml$/i,
        use: ['xml-loader'],
      },
      {
        test: /\.json|geojson$/,
        type: 'json',
      }
    ],
  },
};
